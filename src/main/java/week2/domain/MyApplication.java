package week2.domain;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private ArrayList<User> users = new ArrayList<>();
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private User user;

    private void addUser(User user) {
        users.add(user);
    }

    public boolean checkUsername(String username, ArrayList<User> users){
        for(User user: users){
            if(user.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile();
            }
        }
    }

    public void userProfile() throws IOException {
        while (true)
        {
            if (signedUser == null)
            {
                break;
            }
            System.out.println("1. My Info");
            System.out.println("2. Change Password");
            System.out.println("3. Add admin access");
            System.out.println("4. Log off");
            int choice = sc.nextInt();
            if(choice == 1){
                Info(signedUser);
            };
            if(choice == 2) {
                System.out.println("Enter a new password:");
                Password newpassword = new Password(sc.next());
                signedUser.getPassword().changePassword(signedUser, newpassword);
                for(int i = 0; i < users.size(); i++)
                {
                    if(users.get(i).getId() == signedUser.getId())
                    {
                        users.get(i).equals(signedUser);
                    }
                }
                saveUserList();
            }
            if(choice == 3) addAdmin(signedUser);
            if(choice == 4) logOff();
        }
    }

    private void addAdmin(User signedUser) {
        System.out.println("Enter your password:");
        Password password = new Password(sc.next());
        while(!password.checkPassUser(signedUser.getUsername(), password, users)){
            System.out.println("Password is wrong! Please, try again...");
            password = new Password(sc.next());
        }
        for(User user : users)
        {
            if(user.getUsername().equals(signedUser.getUsername()) && user.getPassword().getPassword().equals(password.getPassword()))
            {
                signedUser = user;
            }
        }
        Admin a1 = new Admin(signedUser.getName(),signedUser.getSurname(),signedUser.getUsername(),signedUser.getPassword(), true);
        System.out.println(a1);
    }

    private void logOff() {
        signedUser = null;
    }

    private void authentication() throws IOException {
        while (true) {
            System.out.println("1. Sign In");
            System.out.println("2. Sign Up");
            System.out.println("3. Exit");
            int choice = sc.nextInt();
            if (choice == 1) signIn();
            if (choice == 2) signUp();
            else break;
        }
    }

    private void signIn() throws IOException {
        System.out.println("Enter your nickname:");
        String username = sc.next();
        while(!checkUsername(username, users)){
            System.out.println("Incorrect username! Please, try again...");
            username = sc.next();
        }
        System.out.println("Enter your password:");
        Password password = new Password(sc.next());
        while(!password.checkPassUser(username, password, users)){
            System.out.println("Password is wrong! Please, try again...");
            password = new Password(sc.next());
        }
        for(User user : users)
        {
            if(user.getUsername().equals(username) && user.getPassword().getPassword().equals(password.getPassword()))
            {
                signedUser = user;
            }
        }
        userProfile();
    }

    private void signUp() throws IOException {
        System.out.println("Enter your name:");
        String name = sc.next();
        System.out.println("Enter your surname:");
        String surname = sc.next();
        System.out.println("Enter your username:");
        String username = sc.next();
        // Check if username is already taken
        while(checkUsername(username, users)){
            System.out.println("This username is already taken. Please, try again...");
            username = sc.next();
        }
        System.out.println("Enter your password:");
        Password password = new Password(sc.next());
        while(!password.checkPassword()) {
            System.out.println("Incorrect password! Please, try again...");
            password = new Password(sc.next());
        }
        User user = new User(name, surname, username, password);
        signedUser = user;
        addUser(user);
        saveUserList();
        userProfile();
    }

    public void start() throws IOException {
        File file = new File("C:\\Users\\Legion\\Desktop\\hw2\\Week2\\src\\main\\java\\week2\\domain\\db.txt");
        Scanner fsc = new Scanner(file);
        // fill userlist from db.txt
        while(fsc.hasNext()){
            int id = fsc.nextInt();
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password = new Password(fsc.next());
            User user = new User(id, name, surname, username, password);
            users.add(user);
        }
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }
        // save the userlist to db.txt
    }
    private void Info(User user){
        System.out.println("Your name: " + user.getName() + "\n" + "Your surname: " + user.getSurname() + "\n" + "Your username: " + user.getUsername() + "\n" + "Your password: " + user.getPassword().getPassword());
    }

    private void saveUserList() throws IOException {
        String data = "";
        for(User user: users){
            data = data + user.getId() + " " + user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPassword().getPassword() + "\n";
        }
        Files.write(Paths.get("C:\\Users\\Legion\\Desktop\\hw2\\Week2\\src\\main\\java\\week2\\domain\\db.txt"), data.getBytes());
    }
}