package week2.domain;
import java.util.ArrayList;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String passwordStr;

    public Password(String passwordStr){
        setPassword(passwordStr);
    }
    public void setPassword(String passwordStr){
        this.passwordStr = passwordStr;
    }
    public String getPassword(){
        return passwordStr;
    }
    boolean checkPassword(){
        if (passwordStr.length() < 9) return false;
        else {
            int i = 0;
            while (!isDigit(passwordStr.charAt(i))){
                if(i > passwordStr.length())
                {
                    return false;
                }
                i++;
            }
            i = 0;
            while (!isUpper(passwordStr.charAt(i))){
                if(i > passwordStr.length()){
                    return false;
                }
                i++;
            }
            i = 0;
            while (!isLower(passwordStr.charAt(i))){
                if(i > passwordStr.length()){
                    return false;
                }
                i++;
            }
            return true;
        }
    }
    boolean isDigit(char n){
        for(char i = '0'; i <= '9'; i++){
            return true;
        }
        return false;
    }
    boolean isUpper(char n){
        for(char i = 'A'; i <= 'Z'; i++){
            return true;
        }
        return false;
    }
    boolean isLower(char n){
        for(char i = 'a'; i <= 'z'; i++){
            return true;
        }
        return false;
    }
    public boolean checkPassUser(String username, Password password, ArrayList<User> users){
        for(User user: users){
            if(user.getUsername().equals(username)) {
                if(user.getPassword().getPassword().equals(password.getPassword())){
                    return true;
                }
            }
        }
        return false;
    }
    public void changePassword(User signedUser, Password password){
        if (password.getPassword() != signedUser.getPassword().getPassword())
        {
            signedUser.getPassword().setPassword(password.getPassword());
        }
        else
        {
            System.out.println("Your new password matches the current password. Please, try again...");
            changePassword(signedUser, password);
        }
        System.out.println("Your new password: " + signedUser.getPassword().getPassword());
    }
}