package week2.domain;

public class User {
        // id (you need to generate this id by static member variable)
        // name, surname
        // username
        // password
        private int id;
        private static int id_gen = 0;
        private static int max = 0;
        private String name;
        private String surname;
        private String username;
        private Password password;

        public User(String name, String surname, String username, Password password){
            this();
            setPassword(password);
            setName(name);
            setSurname(surname);
            setUsername(username);
        }
        public User(int id, String name, String surname, String username, Password password){
            if (max < id)
            {
                max = id;
            }
            id_gen = max;
            id_gen++;
            setId(id);
            setName(name);
            setSurname(surname);
            setUsername(username);
            setPassword(password);
        }
        public User(){
            id = id_gen++;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return name;
        }
        public void setId(int id){
            this.id = id;
        }
        public int getId(){
            return id;
        }
        public void setSurname(String surname){
            this.surname = surname;
        }
        public String getSurname(){
            return surname;
        }
        public void setUsername(String username){
            this.username = username;
        }
        public String getUsername(){
            return username;
        }
        public void setPassword(Password pass){
            this.password = pass;
        }
        public Password getPassword() {
            return password;
        }
    }

