package week2.domain;

public class Admin extends User {
    private boolean admin_access;

    public Admin(String name, String surname, String username, Password password, boolean admin_access){
        super(name, surname, username, password);
        setAccess(admin_access);
    }

    public Admin(boolean admin_access){
        setAccess(admin_access);
    }

    public void setAccess(boolean admin_access){
        this.admin_access = admin_access;
    }

    public boolean isAdmin_access(){
        return admin_access;
    }

    @Override
    public String toString() {
        return "Admin access: " + (admin_access ? "Successfully added" : "Unsuccesfully added");
    }
}
